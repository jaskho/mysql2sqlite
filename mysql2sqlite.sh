#!/bin/bash

#
# Convert mysql database to sqlite, with special attention to supporting
# issues encountered while porting a working Drupal 7 site.
#
# The bulk of this script is from https://gist.github.com/esperlu/943776
# (See ORIGINAL SCRIPT NOTES, below)
#
# CHANGES VERSUS ORIGINAL SCRIPT
#  + Carriage returns embedded in string data
#      sqlite3 removes instances of the \r escape sequence from strings.
#      This causes problems where string data represents PHP serialized
#      strings, as it throws off the character count. We address this
#      by converting 'line 1\r\nline2' to 'line 1' || X'0D' || 'line 2'
#  + Remove column comments
#  + Null bytes embedded in string data
#      sqlite3 removes instances of null characters from strings.
#      This causes problems where string data represents PHP serialized
#      objects, which use null bytes to flag class names. We address
#      this as with carriage returns (see note, above).
#  + AUTO_INCREMENT fields
#      A pre-pass through awk was added in order to avoid making the
#      logic in the main pass unduly complicated.
#      NOTE: as constructed, this script does NOT handle AUTO_INCREMENT fields
#            that are part of a multi-column primary key (hopefully
#            not a common requirement)
#  + Avoid name collisions between indexes and tables by prepending "idx_"
#      to CREATE INDEX lines
#
# Also allows for alternative calling methods (more info under USAGE, below):
#  + MySql database dump (as per the original script)
#  + MySql dump file
#  + SQL string (useful for debugging)
#
#  NOTE: In order to support this, it was necessary to change the scripting
#        shell from sh to bash in order to work on systems running Dash
#        (Debian Squeeze, namely).  Dash, on returning the result of
#        command substitions, interprets '\r' and '\n' as escape sequences
#        instead of string literals.)
#


# --------------------------------------------------------
# ORIGINAL SCRIPT NOTES
# Converts a mysqldump file into a Sqlite 3 compatible file. It also extracts the MySQL `KEY xxxxx` from the
# CREATE block and create them in separate commands _after_ all the INSERTs.
#
# Awk is choosen because it's fast and portable. You can use gawk, original awk or even the lightning fast mawk.
# The mysqldump file is traversed only once.
#
# Thanks to and @artemyk and @gkuenning for their nice tweaks.
# --------------------------------------------------------


# ---------------------------------------------------------
#
#   USAGE:
#
#   Option 1 - Directly use a mysql database
#
#        $ ./mysql2sqlite mysqldump-opts db-name | sqlite3 database.sqlite
#
#        EXAMPLE
#        Assuming...
#          + mysql database name:   myDbas
#          + mysql username:        dbuser
#          + mysql password:        MySecretPassWord
#          + sqlite database name:  database.sqlite
#          + You want to migrate schema but not data (to migrate schema
#               and data, don't use the --no-data mysqldump option)
#
#        $ ./mysql2sqlite --no-data -u root -pMySecretPassWord myDbase | sqlite3 database.sqlite
#
#
#   Option 2 - Pass in a sql dump file
#
#        $ ./mysql2sqlite dumpfile.sql | sqlite3 database.sqlite
#
#        EXAMPLE:
#        $ mysql2sqlite.sh dbdump.sql | sqlite3 database.sqlite
#
#          To create a properly formatted dump file, follow this:
#          $ mysqldump --compatible=ANSI --skip-extneded-insert \
#          > -u root -pMySecretPassWord myDbase > dbdump.sql
#
#
#   Option 3 - Pass in a string (useful for debugging)
#
#        EXAMPLE:
#        $ mysql2sqlite.sh "CREATE TABLE ..." | sqlite3 database.sqlite
#
#         or
#
#        $ sql='CREATE TABLE ...'
#        $ mysql2sqlite.sh "$sql" | sqlite3 database.sqlite
#
# ---------------------------------------------------------


#
# Parse arguments and load source script accordingly
#

z=$1

# Get mysqldump (checks for at least one passed in option, e.g. '-user')
if [ `expr substr "${z}" 1 1` = "-" ] ; then
  q=`mysqldump  --compatible=ansi --skip-extended-insert --compact  "$@"`

# Read from a file
elif [ -f "$1" ]; then
  q=`cat $1`

# Use a string
elif [ $# -eq 1 ]; then
  q="$z"

else
  echo 'ARGMENTS ERROR'
  echo 'Refer to the usage notes in the script header comments'
  exit 1
fi


#
# Pass source script to awk
#

# echo -E "$q"
# exit 0
echo -E "$q" | \



#
# FIRST PASS: Replace PRIMARY KEY if there is auto_increment
#

awk '
BEGIN {
	# Iterate through CREATE TABLE blocks
	RS="\nCREATE TABLE "
	firstRecord = 1
}

# Add back in the "RS" string, which awk skips over
/./ {
	if ( ! firstRecord ) {
		sub( /^/, "CREATE TABLE " )
	} else {
		firstRecord = 0
	}
}

#  Test this table for auto_increment PK fld
/NOT NULL (auto_increment|AUTO_INCREMENT)/ {
		#gsub( /,\n  PRIMARY KEY  \(\"[a-z_]+\"\)/, "" )

	errMsg = ""
	colName = ""
	pkLine = ""

	# Get column name
	#  First, grab the relevant line (makes the regex simpler)
	if ( match( $0, /[\n] [^\n]*AUTO_INCREMENT[^\n]*/) ) {
		pkLine = substr( $0, RSTART+1, RLENGTH-1 )
		#gsub( /.*/, "col line: " colName )
		if ( match( pkLine, /\"[^\"]*\"/) ) {
			colName = substr( pkLine, RSTART+1, RLENGTH-2 )
			# Delete PRIMARY KEY definition
			#   NOTE: we are not handling cases where the auto_increment column is part
			#   of a mullticol pk
			sub( /,\n[ ]*PRIMARY KEY[^\n]*/, "," )

			# Convert auto_increment column def to sqlite3 syntax
			aicolpatt = "\n[ ]*" "\\" "\"" colName "\\" "\"" "[^\n]*"
			sub( aicolpatt, "\n  " "\"" colName "\"" " INTEGER PRIMARY KEY AUTOINCREMENT,")
		} else {
			errMsg="Error extracting AUTO_INCREMENT column name"
		}
	} else {
		errMsg = "Error parsing AUTO_INCREMENT column"
	}

	print
	if ( errMsg ) {
		print "MYSQL2SQLITE_PARSE_ERROR: " errMsg
	}
	next
}
/.*/ {
  print
}' | \


#
# SECOND PASS: Do everything else
#

awk '

BEGIN {
	FS=",$"
	print "PRAGMA synchronous = OFF;"
	print "PRAGMA journal_mode = MEMORY;"
	print "BEGIN TRANSACTION;"
}

# CREATE TRIGGER statements have funny commenting.  Remember we are in trigger.
/^\/\*.*CREATE.*TRIGGER/ {
	gsub( /^.*TRIGGER/, "CREATE TRIGGER" )
	print
	inTrigger = 1
	next
}

# The end of CREATE TRIGGER has a stray comment terminator
/END \*\/;;/ { gsub( /\*\//, "" ); print; inTrigger = 0; next }

# The rest of triggers just get passed through
inTrigger != 0 { print; next }

# Skip other comments
/^\/\*/ { next }

# Print all `mysql2sqlite error` lines (from AUTOINCREMENT parsing script)
# This will cause sqlite3 import to fail, hopefully giving user enough info
# to locate the error message.
/^MYSQL2SQLITE_PARSE_ERROR:/ {
	print
	next
}

# Print all `INSERT` lines. The single quotes are protected by another single quote.
/INSERT/ {
	gsub( /\\\047/, "\047\047" )
	gsub(/\\n/, "\n")

	# Carriage return: break string, concatenating with hex carriage return code
	#  Note: the code from original script:
	#     gsub(/\\r/, "\r")
	#  outputs \r, which for some reason,
	#  sqlite3 ignores (The character is not inserted into the database,
	#  at least in some instances. This may be configuration-specific, or
	#  addressable via sqlite3 options, but I was unable to find another
	#  solution that worked.)
	#  This causes problems for php when CRs are embedded in serialized
	#  strings, as it throws the character count off.
	gsub(/\\r/, "\047 || X\0470D\047 || \047")

	# Null bytes: break string and concat with hex null byte code
	#  This occurs in PHP serialized object; for instance, the class name is
	#  prepended with an "*" wrapped in null bytes.
	gsub(/\\0/, "\047 || X\04700\047 || \047")

	# mysqldump escapes quotes, as in \"string\"; convert to "string"
	gsub(/\\"/, "\"")
	gsub(/\\\\/, "\\")
	gsub(/\\\032/, "\032")
	print
	next
}

# Print the `CREATE` line as is and capture the table name.
/^CREATE/ {
	print
	if ( match( $0, /\"[^\"]+/ ) ) tableName = substr( $0, RSTART+1, RLENGTH-1 )
}

# Replace `FULLTEXT KEY` or any other `XXXXX KEY` except PRIMARY by `KEY`
/^  [^"]+KEY/ && !/^  PRIMARY KEY/ { gsub( /.+KEY/, "  KEY" ) }

# Get rid of field lengths in KEY lines
/ KEY/ { gsub(/\([0-9]+\)/, "") }

# Print all fields definition lines except the `KEY` lines.
/^  / && !/^(  KEY|\);)/ {
	# Remove column comments
	gsub( / COMMENT \047[^\047]*\047/, "" )
	gsub( /AUTO_INCREMENT|auto_increment/, "" )
	gsub( /(CHARACTER SET|character set) [^ ]+ /, "" )
	gsub( /DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP|default current_timestamp on update current_timestamp/, "" )
	gsub( /(COLLATE|collate) [^ ]+ /, "" )
	gsub(/(ENUM|enum)[^)]+\)/, "text ")
	gsub(/(SET|set)\([^)]+\)/, "text ")
	gsub(/UNSIGNED|unsigned/, "")
	if (prev) print prev ","
	prev = $1
}

# `KEY` lines are extracted from the `CREATE` block and stored in array for later print
# in a separate `CREATE KEY` command. The index name is prefixed by the table name to
# avoid a sqlite error for duplicate index name.
# Also prepend with "idx_" to handle situations where a table named {tableName}_{indexName}
# exists (this happens!)
/^(  KEY|\);)/ {
	if (prev) print prev
	prev=""
	if ($0 == ");"){
		print
	} else {
		if ( match( $0, /\"[^"]+/ ) ) indexName = substr( $0, RSTART+1, RLENGTH-1 )
		if ( match( $0, /\([^()]+/ ) ) indexKey = substr( $0, RSTART+1, RLENGTH-1 )
		key[tableName]=key[tableName] "CREATE INDEX \"idx_" tableName "_" indexName "\" ON \"" tableName "\" (" indexKey ");\n"
	}
}

# Print all `KEY` creation lines.
END {
	for (table in key) printf key[table]
	print "END TRANSACTION;"
}
'
exit 0
